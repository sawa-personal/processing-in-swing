//*******************************************************
// Main.java
// created by Sawada Tatsuki on 2018/03/28.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* JavasFXで作成したウィンドウにprocessingを埋め込む */

import java.awt.BorderLayout;
import java.awt.Canvas;
import javax.swing.*;
import processing.core.*;

public class Main {
	//コンストラクタ
	Main() {
		JFrame frame = new JFrame();
		frame.setSize(800, 600);
		PApplet second = new Applet(frame);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true); //フレームを表示
		frame.setResizable(false); //ウィンドウサイズ固定
	}
	
	//メイン関数
	public static void main(String[] args){
		Main main = new Main(); //ウィンドウを表示
	}
}
