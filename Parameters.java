//*******************************************************
// Parameters.java
// created by Sawada Tatsuki on 2018/03/29.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* シミュレータにおけめパラメータをまとめたクラス */

import java.util.*;

public class Parameters {
	public int color = 0; //色

	//色を設定
	public void setColor(int c) {
		if(c < 0 || 255 < c) {
			system.exit(1);
		}
		this.color = c;
	}
}
