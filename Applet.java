//*******************************************************
// Applet.java
// created by Sawada Tatsuki on 2018/03/28.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* Processingによる描画処理を行うクラス */

import java.awt.Canvas;
import java.awt.Graphics2D;
import javax.swing.*;
import processing.core.PApplet;
import processing.core.PSurface;

public class Applet extends PApplet {
	//コンストラクタ
	Applet(JFrame frame) {
		init(frame);
    }
	
    public void settings(){
		size(600, 600);
    }
	
    public void setup(){
		background(0);
		smooth();
		strokeWeight(5);
    }
	
    public void draw(){
		noStroke();
		fill(0, 10);
		rect(0, 0, width, height);
		stroke(255);
		if (mousePressed) {
			line(mouseX, mouseY, pmouseX, pmouseY);
		}
    }

	//主に描画に関する初期化処理
	private void init(JFrame frame) {
		try {
			java.lang.reflect.Method handleSettingsMethod = this.getClass().getSuperclass().getDeclaredMethod("handleSettings");
			handleSettingsMethod.setAccessible(true);
			handleSettingsMethod.invoke(this);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		PSurface surface = super.initSurface();
		Canvas canvas=(Canvas)surface.getNative();
		surface.placeWindow(new int[]{0, 0}, new int[]{0, 0});
		frame.add(canvas);
		this.startSurface();
	}
}
